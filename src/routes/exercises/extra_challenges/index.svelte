<script>
  import Code from "$lib/ui/CodeBlock.svelte";
  import CC from "$lib/ui/CodeInline.svelte";
  import Card from "$lib/ui/Card.svelte";
  import Ex_1 from "./exercises/ex_1.svelte";
  import Ex_2 from "./exercises/ex_2.svelte";
  import Ex_3 from "./exercises/ex_3.svelte";
</script>

<h1>Extra challenges</h1>
<p>
  This page contains several advanced exercises that cover D3's brushing,
  dragging, and zooming functionality. These exercises will not be graded and no
  points will be awarded for them. But, if you are up for it, they are a fun
  challenge!
</p>
<h5>Network visualisations</h5>
<p>
  We have prepared a force-directed network visualization that shows the
  interactions of characters in Star Wars. As in previous exercises, the data is
  loaded in <CC code="ex_1.svelte" /> which then constructs a <CC
    code="_ex_1_network.svelte"
  /> component that contains the actual visualization. Aside from the <CC
    code="forceSimulation"
  /> and the way images are used as fill for some of the circles, most things should
  look familiar at this point. The <CC code="forceSimulation" />, however,
  deservese an explanation:
</p>
<Code
  language="javascript"
  code={`const simulation = forceSimulation(nodes)
    .force('link', forceLink(links).id((d) => d.label))
    .force('charge', forceManyBody())
    .force('collide', forceCollide((d) => rScale(d.value)))
    .force('center', forceCenter(width / 2, height / 2))
    .on('tick', () => {
      // Needed so that svelte detects the values have changed...
      nodes = nodes;
      links = links;
    });`}
/>
<p>
  We use d3.js' <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-force">d3-force</a
  > module for running force simulations, which compute how a network should be layed
  out so that it is most legible. Essentially, the simulation tries to minimize the
  forces we introduce. Several forces are provided in the module, we used the:
</p>
<ul>
  <li>
    <CC code="forceLink" /> which introduces a force that pulls nodes that are connected
    together.
  </li>
  <li>
    <CC code="forceManyBody" /> which introduces a force that pushes all nodes away
    from each other.
  </li>
  <li>
    <CC code="forceCollide" /> which introduces a force that pushes nodes that overlap
    away from each other.
  </li>
  <li>
    <CC code="forceCenter" /> which introduces a force that pulls all nodes to the
    center.
  </li>
</ul>
<p>
  To minimize the forces in the layout, the simulation uses a temperature
  schedule that is controlled throught the <CC code=".alpha()" /> helper function.
  Initially the temperature is high, allowing the nodes to move faster. As the simulation
  runs, the temperature decreases, until it hits zero. Then the simulation stops.
</p>
<p>
  While the simulation is running, it will continually release a <CC
    code="tick"
  /> event. We can monitor that event to re-draw the nodes and edges while the simulation
  is running. In Svelte, our <CC code="tick" /> callback actually only has to tell
  Svelte that the values within the <CC code="nodes" /> and <CC code="links" /> arrays
  have changed. We do that by assigning both arrays to themselves, as Svelte uses
  assignment to detect changing variables. The simulation will have added and update
  several fields on the nodes and links:
</p>
<ul>
  <li><CC code="node.x" /> the x-coordinate of the node.</li>
  <li><CC code="node.y" /> the y-coordinate of the node.</li>
  <li><CC code="node.vx" /> the x-velocity of the node.</li>
  <li><CC code="node.vy" /> the y-velocity of the node.</li>
  <li>
    <CC code="node.fx" /> and <CC code="node.fy" />. When set to a non-null
    value, the node will be forced to stay at these coordinates and drags all
    nodes connected to it towards that location.
  </li>
  <li>
    <CC code="link.source" /> and <CC code="link.target" /> point to the nodes that
    this link connects, not just their index or id, but the actual variables! So,
    you can get the coordinates to draw the link using <CC
      code="link.source.x"
    />, ... .
  </li>
</ul>
<p>
  In the following exercises you will extend the visualization with three
  interactions:
</p>
<ol>
  <li>Support dragging nodes so a user can manipulate the layout.</li>
  <li>Support zooming and panning to better inspect the network.</li>
  <li>
    Highlight the nodes and links connected to a node when you hover over a
    node.
  </li>
</ol>
<p>
  You will develop these interactions in separate JavaScript files (
  <CC code="_ex_1_drag.js" />, <CC code="_ex_2_zoom.js" />, and <CC
    code="_ex_3_hover.js"
  />). This allows us to re-use some of the functionality later on. However, we
  will have to do a bit more work to use Svelte's reactivity. But that is worth
  the trouble!
</p>

<h5>Exercise 1 (0 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/extra_challenges/exercises/_ex_1_network.svelte"
  /> and <CC code="src/routes/extra_challenges/exercises/_ex_1_drag.js" />. Any
  changes you make to those file should show up below.
</p>
<p>
  d3.js provides the <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-drag">d3-drag</a
  >
  module to add dragging functionality to DOM-elements. As you probably expect by
  now, to use the module, you have to import, call, and configure the <CC
    language="javascript"
    code="drag()"
  /> function. This gives back a function that takes a
  <em>selected</em> DOM-handle and makes that element draggable. Like with the brush
  in exercise 3 of week 7, you can configure what has to happen when the drag starts,
  moves, and ends.
</p>
<p>
  For a nice dragging behaviour on force-directed networks, you have to increase
  the temperature of the simulation to <CC code="0.3" /> when a drag starts and keep
  it at that level while the drag moves. This ensures the layout gets a chance to
  adapt. To actually move a node, you have to force it to the position of the mouse
  while the drag moves.
  <em>Hint: check the argument your callbacks are given when called!</em>
  Finally, when the drag ends, you have to release the node. The simulation will
  continue for a short while settling in to the new layout, you do not have to stop
  it when the drag ends.
</p>
<p>
  With this information, implement a re-usable dragging function in <CC
    code="_ex_1_drag.js"
  />. The file should export a function that takes a force-simulation as
  argument and gives you a function to use as an
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#template-syntax-element-directives-use-action"
    >action</a
  >
  on the circle elements. That action-function should take each circle's <CC
    code="node"
  /> variable as second argument. Configure the dragging behaviour within the action-function
  as described and use it in <CC code="_ex_1_network.svelte" />. Remember that
  an action-function is a function that takes a handle to an element on the DOM
  as argument. For example, when you specify <CC
    language="svelte"
    code="<circle use:func />"
  /> Svelte will call <CC language="javascript" code="func(circleHandle)" /> when
  the circle is constructed. To pass more arguments to an action-function, you can
  use <CC language="svelte" code={`<circle use:func=\{value\} />`} />, which
  will call <CC language="javascript" code="func(circleHandle, value)" />.
</p>

<Card><Ex_1 /></Card>

<h5>Exercise 2 (0 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/extra_challenges/exercises/_ex_2_network.svelte"
  /> and <CC code="src/routes/extra_challenges/exercises/_ex_2_zoom.js" />
  />. Any changes you make to those file should show up below.
</p>
<p>
  For zooming and panning, we will use d3.js' <a
    target="_blank"
    class="link-secondary"
    href="https://github.com/d3/d3-zoom">d3-zoom</a
  >
  module. Using this module is similar to the brush and drag modules. To configure
  it, you have to specify the size of the SVG element using the <CC
    code=".extent()"
  /> function and register a callback for the <CC code="zoom" /> event. This event
  is released when the user zooms or drags on the element you applied the zoom-function
  to. The
  <CC code="transform" /> property of the argument that is passed to your callback
  specifies the SVG transform that corresponds to the user's movement. We want to
  capture that <CC code="transform" /> and have Svelte automatically use new values
  when they occur.
</p>
<p>
  If we were to implement this functionality directly within <CC
    code="_ex_2_network.svelte"
  />, you could use the same approach as with the brush in Exercise 3 (where we
  updated the <CC code="range" /> variable and had svelte react to the changes).
  For this exercise, however, you have to implement it in <CC
    code="_ex_2_zoom.js"
  />. Svelte cannot monitor if variables in other files change, except when you
  use a
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#run-time-svelte-store">store</a
  >. For this exercise, you will use a <CC code="readable" /> store:
</p>
<Code
  language="javascript"
  code={`const myStore = readable(new Date(), function start(set) {
  const handle = setInterval(set(new Date()), 1000);
  return function stop() {
    clearInterval(handle);
  }
});`}
/>
<p>
  Readable stores are used to give acces to values that change, but cannot be
  changed by the components that use them. For example, they can store the
  current time, as in the example above. Here, we created readable store with
  current time as initial value. The second argument is a function that is
  called the moment a first component starts using the value of the store. In
  that function, we update the value of the store every second by using the <CC
    code="set"
  /> function that is given as argument to the start function. Finally, the start
  function returns a function that performs any cleanup that has to happen when the
  last component stops using the store. Here, we cleared the interval so that the
  store's value is not updated every second anymore.
</p>
<p>
  Using this information, implement zooming and panning functionality in <CC
    code="_ex_2_zoom.js"
  />. The file should export a function that takes the width and height of the
  element it will be applied to and return an action-function that configures
  the zoom-module. Define a readable store that monitors the <CC
    code="transform"
  /> value given in the <CC code="zoom" /> event callback.
  <em>
    Hint: you need to use the store's set function in the <CC code="zoom" /> callback!
  </em>
  Assign the store as a property of the action-function, so that it can be accessed
  later on.
  <em>
    Hint: Functions in JavaScript are really just objects that you can call, so
    you can give properties using:
  </em>
</p>
<Code
  language="javascript"
  code={`function func() {
  return 'func called!';
}
func.myProperty = 'func property accessed!';
console.log(func());
console.log(func.myProperty);`}
/>
<p>
  Use the zoom-action you have created on the SVG-element in <CC
    code="_ex_2_network.svelte"
  />. Apply the transform to the SVG group tag that contains the nodes and links
  and not on the SVG element. This makes sure that the Star Wars logo does not
  move when you zoom or pan!
</p>
<Card><Ex_2 /></Card>

<h5>Exercise 3 (0 pt.)</h5>
<p>
  For this exercise, you have to edit the files <CC
    code="src/routes/extra_challenges/exercises/_ex_3_network.svelte"
  /> and <CC code="src/routes/extra_challenges/exercises/_ex_3_hover.js" />
  />. Any changes you make to those file should show up below.
</p>
<p>
  The hover interaction is a bit different from the other two, as we do not need
  to use any d3.js functionality. Instead we can just use Svelte's <CC
    code="on:mouseover"
  /> and
  <CC code="on:mouseout" /> directives to capture the user's interaction. The complexity
  of this interaction is in determining which nodes and links should be highlighted
  and which ones should fade.
</p>
<p>
  Because you have to implement the hover functionality in <CC
    code="_ex_3_hover.js"
  />, you will have to use
  <a
    target="_blank"
    class="link-secondary"
    href="https://svelte.dev/docs#run-time-svelte-store">stores</a
  >
  again to make Svelte detect when values change. In this case, you will need both
  <CC code="writable" /> and <CC code="derived" /> stores. <CC
    code="writable"
  /> stores are the easiest type of store in Svelte, you can just initialize them
  with a value and then assign to them using the <CC code="$" /> notation:
</p>
<Code
  language="javascript"
  code={`// Create the writable store
const myStore = writable('initial value');
// Update the value
$myStore = 'new value!';
// Print the value when the value changes
$: console.log($myStore);`}
/>
<p>
  <CC code="derived" /> stores can be used to compute values that depend on the value
  of other stores. For instance, if we have a store containing the grades of a student,
  then a derived store can be used to automatically update that student's average
  grade:
</p>
<Code
  language="javascript"
  code={`import { mean } from "d3-array"; 
const average_grade = derived(grades, ($grades) => {
  return mean($grades);
});`}
/>
<p>
  Here, a derived store is constructed by giving it the store that it depends on
  and a callback function that uses that store's value to computed the derived
  store's value. Specifically, <CC code="grades" /> is a store containing the grades
  of a student as an array of numerical values and the callback computes the mean
  grade for that student. It is a convention to name the variable within the callback
  as the name of the store with a <CC code="$" /> prefixed (i.e., <CC
    code="$grades"
  />), but the <CC code="$" /> does not have any special meaning here. It just indicates
  that you are working with the value of a store and not the store object itself!
</p>
<p>
  Aside from <CC language="javascript" code=".filter()" />, which we showed
  before, two additional array manipulation functions are often useful in
  derived stores:
  <CC language="javascript" code=".map()" /> and <CC
    language="javascript"
    code=".reduce()"
  />. They are used to replace typical for-loop patterns. The <CC
    language="javascript"
    code=".map()"
  /> replaces a loop that creates a single value for each item in an array. For example,
</p>
<Code
  language="javascript"
  code={`const xs = nodes.map(d => d.x);
`}
/>
<p>creates an array containing the x-coordinates of the nodes.</p>
<p>
  <CC language="javascript" code=".reduce()" /> is more flexible, as it does not
  have to return a value for every item in the array. Instead, you get control over
  what to add to the output and what the output should be. For example, a <CC
    language="javascript"
    code=".reduce()"
  /> can be used to sum all elements in an array:
</p>
<Code
  language="javascript"
  code={`const sum = array.reduce((output, current_value) => {
    return output += current_value;
  }, 0);`}
/>
<p>
  Here, the first argument is a callback that is called on every item in the
  array. This callback takes as arguments the variable that will be the output
  and the current value in the array. Within this callback you can update the
  output variable, here we simply add the current value to it. The callback has
  to return the updated output variable, as it will be used in the next
  iteration. The second argument is the output's initial value. In this example
  we used zero as initial value. A <CC language="javascript" code=".reduce()" />
  can also be used to convert an array into an object, which is sometimes useful:
</p>
<Code
  language="javascript"
  code={`const object = array.reduce((output, current_item) => {
    output[current_item.id] = current_item;
    return output;
  }, {});`}
/>
<p>
  <em>
    Note, objects cannot be used as keys in normal JavaScript objects, that is
    why we used the <CC code="id" /> field in this example!
  </em>
</p>
<p>
  Using this information, complete the hover functionality in <CC
    code="_ex_3_hover.js"
  />. The file already exports a function that takes the <CC code="nodes" /> and
  <CC code="links" /> arrays, computes each node's neighbors, and contains a writable
  store to track the currently hovered node. Define two derived stores that contain
  a <CC language="javascript" code="Set" /> indicating which nodes and links should
  be highlighted. <em>Hint: look up how sets work in JavaScript!</em> These sets
  should contain all nodes and all links when there is no currently hovered
  node. Otherwise, they should contain only the hovered node and its neighbors
  and the links between them. Add these derived stores to the object that is
  returned from the function. Finally, use the hovering functionality in <CC
    code="_ex_3_network.js"
  />, add mouse event listeners to the circles that update the hover state, and
  set the node's and link's opacity to <CC code="0.3" /> when they are not in the
  sets.
</p>
<Card><Ex_3 /></Card>

<h5>Better data loading</h5>
<p>
  The way that data was loaded in these exercises is not optimal. You have
  probably noticed that the page jumps around when it is refreshed. This happens
  for two reasons: (1) the loading-messages do not have the same size as the
  visualizations, and (2) the visualisations are not pre-rendered by our server.
</p>
<p>
  There are two ways we could solve this issue. The first approach removes the
  if-block we have used so far. As a result, components that depend on the data
  are shown regardless of whether the data is available yet. This means that we
  have deal with the case that our data is not available within those
  components. They have to update themselves when the data becomes available.
  You have enough Svelte experience by now to figure out how to do this.
  However, this approach complicates the code in your components. In addition,
  it breaks the spirit of the <a
    target="_blank"
    class="link-secondary"
    href="https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization"
    >RAII principle</a
  >: objects should acquire their resources during their initialization and
  release them when they get destroyed.
</p>
<p>
  The second approach relies on SvelteKit rather than pure Svelte. Unlike
  Svelte, SvelteKit is able initialize components with pre-loaded values. We
  only have to tell it how it should <a
    target="_blank"
    class="link-secondary"
    href="https://kit.svelte.dev/docs/loading">load</a
  >
  our data! Unfortunately, this only works when a component is shown as a page and
  not when it is imported and used on a page. So, to see this approach in action,
  you have to look at the
  <a
    target="_blank"
    class="link-secondary"
    href="/exercises/extra_challenges/exercises/bonus">bonus exercise</a
  >
  directly. You can also compare it to
  <a
    target="_blank"
    class="link-secondary"
    href="/exercises/extra_challenges/exercises/ex_1">exercise 1</a
  >. The difference is most obvious when you refresh both pages a couple of
  times!
</p>
<p>
  To tell SvelteKit how to load our data, we have to define a module-script in
  the component:
</p>
<Code
  language="svelte"
  code={`<\script context="module">
  export async function load({ fetch }) {
    const response = await fetch("/data/starwars.json");

    return {
      status: response.status,
      props: {
        data: response.ok && (await response.json())
      }
    };
  }
</h5\script>`}
/>
<p>
  This should be an additional script-tag within the component, separate from
  the ones we have used in components so far! Within that module-script, you can
  export an asynchronous function called <CC
    language="javascript"
    code="load()"
  />. This function is given an object as input, but we only need the <CC
    code="fetch"
  /> property, which we extract using object destructuring. Within the load function,
  we can use <CC language="javascript" code="fetch()" /> to load our file. You should
  not use the d3.js loading functions here because they use the native fetch funcion
  and not the one given by SvelteKit. Finally, we have to return an object with two
  values: <CC code="status" /> and <CC code="props" />. The status value comes
  directly from the <CC code="response" /> we got from fetch. Within <CC
    code="props"
  /> we can specify values for the component properties that our component should
  be initialized with. Here, we only need a value for the <CC code="data" /> array.
  To get that value, we interpret the text contained in <CC code="response" /> as
  json by calling the <CC language="javascript" code=".json()" /> function on it.
  <em
    >Look at the
    <a
      target="_blank"
      class="link-secondary"
      href="https://github.com/d3/d3-dsv">d3-dsv</a
    >
    module if you need to interpret text as csv!
  </em>
  Now, SvelteKit will instantiate our component with the <CC code="data" /> property
  already filled in!
</p>

<p>
  <em>
    There are no exercises about this section. I just really wanted to show how
    you can avoid the page-jumping! Perhaps you can use this approach for your
    projects later in the course :)
  </em>
</p>
